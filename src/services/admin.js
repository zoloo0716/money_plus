const {
  getUsers,
  insertUser,
  updateUser,
  deleteUser,
  login,
  insertTransactions,
  insertWallet,
  insertCategory,
  approveUser,
} = require("../logic/admin_mongo");
const { logger } = require("../common/log");
const { sendEmail } = require("../common/email");

module.exports = function (app, connection) {
  /**
   * GET - Жагсаалт авах, ямар нэг дата харахад ашиглана => app.get()
   * POST - Login, Create дээр ашиглана => app.post()
   * PUT - Update буюу дата засахад ашиглана => app.put()
   * DELETE - Устгахад ашиглана => app.delete()
   */

  app.post("/api/login", async (req, res) => {
    try {
      logger.info(`${req.ip} /api/login [POST]`);

      login(req, res, connection);
    } catch (err) {
      logger.error(`${req.ip} ${err}`);
      res.status(500).json({ error: err.message });
    }
  });

  // endpoints
  app.get("/api/user", async (req, res) => {
    try {
      logger.info(`${req.ip} /user [get]`);

      getUsers(req, res, connection);
    } catch (err) {
      logger.error(`${req.ip} ${err}`);
      res.status(500).json({ error: err.message });
    }
  });

  app.get("/api/approve/:id/:code", async (req, res) => {
    try {
      logger.info(`${req.ip} /api/approval/:id/:code [get]`);
      approveUser(req, res, connection);
    } catch (err) {
      logger.error(`${req.ip} ${err}`);
      res.status(500).json({ error: err.message });
    }
  });

  app.post("/api/user", async (req, res) => {
    try {
      logger.info(`${req.ip} /user [post]`);
      insertUser(req, res, connection);
    } catch (err) {
      logger.error(`${req.ip} ${err}`);
      res.status(500).json({ error: err.message });
    }
  });

  app.put("/api/user", async (req, res) => {
    try {
      logger.info(`${req.ip} /user [put]`);
      updateUser(req, res, connection);
    } catch (err) {
      logger.error(`${req.ip} ${err}`);
      res.status(500).json({ error: err.message });
    }
  });

  app.delete("/api/user", async (req, res) => {
    try {
      logger.info(`${req.ip} /wallet [delete]`);
      deleteUser(req, res, connection);
    } catch (err) {
      logger.error(`${req.ip} ${err}`);
      res.status(500).json({ error: err.message });
    }
  });

  app.post("/api/transaction", async (req, res) => {
    try {
      logger.info(`${req.ip} /transaction [post]`);
      insertTransactions(req, res, connection);
    } catch (err) {
      logger.error(`${req.ip} ${err}`);
      res.status(500).json({ error: err.message });
    }
  });

  app.post("/api/wallet", async (req, res) => {
    try {
      logger.info(`${req.ip} /wallet [post]`);
      insertWallet(req, res, connection);
    } catch (err) {
      logger.error(`${req.ip} ${err}`);
      res.status(500).json({ error: err.message });
    }
  });

  app.post("/api/category", async (req, res) => {
    try {
      logger.info(`${req.ip} /category [post]`);
      insertCategory(req, res, connection);
    } catch (err) {
      logger.error(`${req.ip} ${err}`);
      res.status(500).json({ error: err.message });
    }
  });
};
